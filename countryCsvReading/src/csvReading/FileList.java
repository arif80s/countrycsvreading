package csvReading;

import java.lang.*;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.util.*;

public class FileList {

    CsvToArrayList csvToArrayList = new CsvToArrayList();
    final static List allFieldsArrayList = new ArrayList<String>();
    public static List arrayoflevel = new ArrayList<String>();
    public static List missingFieldArray = new ArrayList<String>();
//    public static List minMax = new ArrayList<Integer>();
    public static List levelMinMax = new ArrayList<Integer>();
    public static int touchedFolder = 0;
    public static int missingCountryArray = 0;
    public static int min = 99999999;
    public static int max = 0;
    public static int levelSize = 0;
    //private static int counter = 0;

    static HashMap<String, Integer> hmapFixedField = new FixedObjectProvider().getHmapFixedField();
    static HashMap<String, HashMap<Integer, Integer>> baseMap = new FixedObjectProvider().getBaseMap();

    public static void listFilesForFolder(final File folder) {
        int counter = 0;
        missingFieldArray.clear();
        //System.out.println(folder.getName());
        for (final File fileEntry : folder.listFiles()) {
            //System.out.println("--------------------");
            //System.out.println(folder.listFiles().length);

            if (fileEntry.isDirectory()) {
//                 System.out.println(folder.listFiles().length);
                //System.out.println(fileEntry.getName());
                //missingCountryArray++;
                listFilesForFolder(fileEntry);
            }//end if (fileEntry.isDirectory())
            else {

//                System.out.println(folder.listFiles().length);
                //System.out.println(fileEntry.getName());
                touchedFolder++;
                counter++;
//                System.out.println(counter++);
                ArrayList<String> levelElement = CsvToArrayList.CSVtoArrayList(fileEntry, counter, folder.listFiles().length);
//                System.out.println(CsvToArrayList.cn);
//                System.out.println("csvName "+ fileEntry.getName());
//                   Iterator itLevel = levelElement.iterator();
//                   while(itLevel.hasNext()){
//                       List<String> a1 = (List<String>) it.next();
//                   }

//              count fixed field occured level wise

                if (levelElement.contains("\"ISO\"")) {
                    HashMap<Integer, Integer> ISOMap = baseMap.get("ISO");
                    int value = ISOMap.get(counter);
                    value++;
                    ISOMap.put(counter, value);
                }
                if (levelElement.contains("\"NAME_ISO\"")) {
                    HashMap<Integer, Integer> ISOMap = baseMap.get("NAME_ISO");
                    int value = ISOMap.get(counter);
                    value++;
                    ISOMap.put(counter, value);
                }
                if (levelElement.contains("\"NAME_LOCAL\"")) {
                    HashMap<Integer, Integer> ISOMap = baseMap.get("NAME_LOCAL");
                    int value = ISOMap.get(counter);
                    value++;
                    ISOMap.put(counter, value);
                }
                if (levelElement.contains("\"NAME_OBSOLETE\"")) {
                    HashMap<Integer, Integer> ISOMap = baseMap.get("NAME_OBSOLETE");
                    int value = ISOMap.get(counter);
                    value++;
                    ISOMap.put(counter, value);
                }
                if (levelElement.contains("\"NAME_ENGLISH\"")) {
                    HashMap<Integer, Integer> ISOMap = baseMap.get("NAME_ENGLISH");
                    int value = ISOMap.get(counter);
                    value++;
                    ISOMap.put(counter, value);
                }

//              maximum missing field frequency determination
                if (!levelElement.contains("\"ISO\"")) {
                    int valueISO = hmapFixedField.get("ISO");
                    valueISO++;
                    hmapFixedField.put("ISO", valueISO);

                }
                if (!levelElement.contains("\"NAME_ISO\"")) {
                    int value = hmapFixedField.get("NAME_ISO");
                    value++;
                    hmapFixedField.put("NAME_ISO", value);

                }
                if (!levelElement.contains("\"NAME_LOCAL\"")) {
                    int value = hmapFixedField.get("NAME_LOCAL");
                    value++;
                    hmapFixedField.put("NAME_LOCAL", value);

                }
                if (!levelElement.contains("\"NAME_OBSOLETE\"")) {
                    int value = hmapFixedField.get("NAME_OBSOLETE");
                    value++;
                    hmapFixedField.put("NAME_OBSOLETE", value);

                }
                if (!levelElement.contains("\"NAME_ENGLISH\"")) {
                    int value = hmapFixedField.get("NAME_ENGLISH");
                    value++;
                    hmapFixedField.put("NAME_ENGLISH", value);

                }

                Set<String> levelSet = new HashSet<String>(levelElement);
                int levelSetSize = levelSet.size();
                //Set<String> fixedLevelSet = new HashSet<String>();
                levelSet.add("\"ISO\"");
                levelSet.add("\"NAME_ISO\"");
                levelSet.add("\"NAME_LOCAL\"");
                levelSet.add("\"NAME_OBSOLETE\"");
                levelSet.add("\"NAME_ENGLISH\"");
//                System.out.println("woah"+ fixedLevelSet);
//                System.out.println("level set ::::"+levelSet);

                int afterAddLevelSetSize = levelSet.size();

                if (levelSetSize != afterAddLevelSetSize) {
                    missingCountryArray++;
//                        System.out.println("\n\n.............................\nMissing some fields....\n");
//                        System.out.println("Country Name : "+CsvToArrayList.cn);
//                        System.out.println("Csv File Name : "+ fileEntry.getName()+"\n.....................");

                    if (missingFieldArray.isEmpty()) {
                        missingFieldArray.add(CsvToArrayList.cn);
                        missingFieldArray.add(fileEntry.getName());
                    } else {
                        missingFieldArray.add(fileEntry.getName());
                    }

                }

//                    for(int i=0; i<5; i++){
//                        levelSet.add(fixedLevelSet)
//                    }
                //System.out.println("level " + counter + " elements::\n");
                for (int j = 0; j < levelElement.size(); j++) {
                    //System.out.println(levelElement.get(j));
                    allFieldsArrayList.add(levelElement.get(j));
                }
//                minimum and maximum level field calculation

                if (levelMinMax.size() < counter) {
                    List minMax = new ArrayList<Integer>();

                    minMax.add(levelElement.size());
                    minMax.add(levelElement.size());
                    levelMinMax.add(minMax);

                } else {
                    List a = new ArrayList<Integer>();
                    a = (List) levelMinMax.get(counter - 1);
                    min = (int) a.get(0);
                    max = (int) a.get(1);
                    if (levelElement.size() < min) {
                        a.add(0, levelElement.size());
                    }
                    if (levelElement.size() > max) {
                        a.add(1, levelElement.size());
                    }
                }

                if (counter > arrayoflevel.size()) {
                    arrayoflevel.add(levelElement);
                } else {
                    List<String> a = (List<String>) arrayoflevel.get(counter - 1);

                    int l;
                    l = levelElement.size();
                    for (int i = 0; i < l; i++) {
                        a.add(levelElement.get(i));
                    }
                }//else of if (counter > arrayoflevel.size()) 

                if (folder.listFiles().length == counter) {
//                            System.out.println("missing country with csvFile,,,,,,"+ missingFieldArray);
                    Iterator it = missingFieldArray.iterator();
                    int aa = 0;
                    while (it.hasNext()) {
                        aa++;
                        if (aa == 1) {
                            System.out.println("\n...................\n\nCountry : " + it.next() + "\n\nmissing files are....\n");
                        } else {
                            System.out.println(it.next());
                        }
                    }
                    //System.out.println("...........................");
                }
            }//else of if (fileEntry.isDirectory())
        }//end of [for (final File fileEntry : folder.listFiles())]
//        System.out.println("total csv file : "+ touchedFolder);
//        System.out.println("total missing csv file : "+ missingCountryArray);
    }//end of method [public static void listFilesForFolder(final File folder)]
    
    public static void printLevelWiseField(){
        System.out.println("grrrrrrrr___--------__"+ baseMap);
    }

    public static void printMissingFieldFrequency() {
        Set set = hmapFixedField.entrySet();
        Iterator iterator = set.iterator();
        while (iterator.hasNext()) {
            Map.Entry mentry = (Map.Entry) iterator.next();
            System.out.print("Field is: " + mentry.getKey() + " & Maximum missing is: ");
            System.out.println(mentry.getValue());
        }
    }

    public static void printMinMax() {
        Iterator it = levelMinMax.iterator();
        int lc = 0;
        while (it.hasNext()) {
            lc++;
            System.out.println("\n\nlevel " + lc + "  MinMax:: ");
            List a1 = (List) it.next();
            for (int i = 0; i < 2; i++) {
                if (i == 0) {
                    System.out.println("Minimum field of level : " + a1.get(i));
                }
                if (i == 1) {
                    System.out.println("Maximum field of level : " + a1.get(i));
                }

            }

//            for (Iterator<String> iterator = foo.iterator(); iterator.hasNext();) {
//                String next = iterator.next();
//                System.out.println(next);
//            }
            System.out.println("....................................\n\n");
        }

    }

    public static void printAccuracy() {
        System.out.println("total csv file : " + touchedFolder);
        System.out.println("total missing csv file : " + missingCountryArray);

        System.out.println((missingCountryArray * 100) / touchedFolder + "% is missing");
    }

    public static void print() {
        Iterator it = arrayoflevel.iterator();
        int lc = 0;
        while (it.hasNext()) {
            lc++;
            System.out.println("\n\nlevel " + lc + "  fields:: ");
            List<String> a1 = (List<String>) it.next();
            Set<String> foo = new HashSet<String>(a1);

            for (Iterator<String> iterator = foo.iterator(); iterator.hasNext();) {
                String next = iterator.next();
                System.out.println(next);
            }
            System.out.println("....................................\n\n");
        }//end of while (it.hasNext()) 
    }//end of public static void print() 
}//end of class [public class FileList]
