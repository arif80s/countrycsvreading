package csvReading;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CsvToArrayList 
{
    public static String cn;
    public static ArrayList<String> CSVtoArrayList(File fileEntry, int counter, int levelFound) 
    {
        BufferedReader crunchifyBuffer = null;
        ArrayList<String> levelResult = new ArrayList<String>();
        ArrayList<String> levelResult1 = new ArrayList<String>();
        String countryName = null;

        try 
        {
            String crunchifyLine = null;
            crunchifyBuffer = new BufferedReader(new FileReader(fileEntry));

            // How to read file in java line by line?
            if (counter == 1) 
            {
                for (int i = 1; i <= 2; i++) {
                    crunchifyLine = crunchifyBuffer.readLine();
                    if (i == 1) {
                        //System.out.println("Raw CSV data: " + crunchifyLine);
                        //System.out.println("Converted ArrayList data: " + crunchifyCSVtoArrayList(crunchifyLine) + "\n");

                        levelResult = crunchifyCSVtoArrayList(crunchifyLine);
                        //System.out.println(levelResult);
                    } 
                    else {
                        levelResult1 = crunchifyCSVtoArrayList(crunchifyLine);
                        CsvToArrayList.cn = levelResult1.get(3);
                        countryName = levelResult1.get(3);
//                        System.out.println("Country::" + countryName);
//                        System.out.println("Level found::" + levelFound);
                    }
                }
            } //if (counter == 1) 
            else 
            {
                crunchifyLine = crunchifyBuffer.readLine();
                levelResult = crunchifyCSVtoArrayList(crunchifyLine);
            }//else of if (counter == 1) 

//            while ((crunchifyLine = crunchifyBuffer.readLine()) != null) {
////                System.out.println("Raw CSV data: " + crunchifyLine);
////                //System.out.println("Converted ArrayList data: " + crunchifyCSVtoArrayList(crunchifyLine) + "\n");
////            
////            levelResult = crunchifyCSVtoArrayList(crunchifyLine);
//            }

                


        } 
        catch (IOException e) 
        {
            e.printStackTrace();
        } 
        finally 
        {
            try 
            {
                if (crunchifyBuffer != null) 
                {
                    crunchifyBuffer.close();
                }
            } 
            catch (IOException crunchifyException) 
            {
                crunchifyException.printStackTrace();
            }
        }//finally 
        return levelResult;
    }//public static ArrayList<String> CSVtoArrayList(File fileEntry, int counter, int levelFound) 

    public static ArrayList<String> crunchifyCSVtoArrayList(String crunchifyCSV) 
    {
        ArrayList<String> crunchifyResult = new ArrayList<String>();
        if (crunchifyCSV != null) 
        {
            String[] splitData = crunchifyCSV.split("\\s*,\\s*");
            for (int i = 0; i < splitData.length; i++) 
            {
                if (!(splitData[i] == null) || !(splitData[i].length() == 0)) 
                {
                    crunchifyResult.add(splitData[i].trim());
                }
            }//for (int i = 0; i < splitData.length; i++) 
        }//if (crunchifyCSV != null) 
        return crunchifyResult;
    }// end method : public static ArrayList<String> crunchifyCSVtoArrayList(String crunchifyCSV) 
}//end class public class CsvToArrayList 
