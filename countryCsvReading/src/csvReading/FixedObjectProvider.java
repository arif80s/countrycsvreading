/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csvReading;

import java.util.HashMap;

/**
 *
 * @author abra
 */
public class FixedObjectProvider {

    static HashMap<String, Integer> hmapFixedField = new HashMap<String, Integer>();
    static HashMap<String, HashMap<Integer, Integer>> baseMap = new HashMap<>();
    static HashMap<Integer, Integer> childMap = new HashMap<>();

    public static HashMap<String, HashMap<Integer, Integer>> getBaseMap() {
        childMap.put(1, 0);
        childMap.put(2, 0);
        childMap.put(3, 0);
        childMap.put(4, 0);
        childMap.put(5, 0);
        childMap.put(6, 0);
        baseMap.put("ISO", childMap);
        baseMap.put("NAME_ISO", childMap);
        baseMap.put("NAME_LOCAL", childMap);
        baseMap.put("NAME_OBSOLETE", childMap);
        baseMap.put("NAME_ENGLISH", childMap);
        return baseMap;
    }

    public static HashMap<String, Integer> getHmapFixedField() {
        hmapFixedField.put("ISO", 0);
        hmapFixedField.put("NAME_ISO", 0);
        hmapFixedField.put("NAME_LOCAL", 0);
        hmapFixedField.put("NAME_OBSOLETE", 0);
        hmapFixedField.put("NAME_ENGLISH", 0);
        return hmapFixedField;
    }

//    public void setHmapFixedField(HashMap<String, Integer> hmapFixedField) {
//        this.hmapFixedField = hmapFixedField;
//    }
}
